﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyCircle : MonoBehaviour
{
    [SerializeField]
    Transform canvas;
    [SerializeField]
    GameObject circlePrefab;
    [SerializeField]
    Transform spawnCenter;
    [SerializeField]
    float spawnRadius = 600f;

    float cost = 100f;
    int boughtCircles = 0;
    Text text;

    void Start()
    {
        text = transform.GetChild(0).GetComponent<Text>();
    }

    public void Buy()
    {
        if (PlayerStats.TryToSpendMoney(cost) && boughtCircles < 5)
        {
            //Spawn the circle around the target at a distance in a random position
            float randomAngle = Random.Range(0, 360);
            Vector2 randomSpawnVector = Quaternion.AngleAxis(randomAngle, Vector3.forward) * new Vector2(spawnRadius, 0);
            randomSpawnVector += new Vector2(spawnCenter.position.x, spawnCenter.position.y);
            Instantiate(circlePrefab, randomSpawnVector, Quaternion.identity).transform.SetParent(canvas);
            boughtCircles++;
            cost *= 10;
            text.text = boughtCircles < 5 ? "Buy Circle\nCost: " + cost.ToString() : "Can't buy more";
        }
    }
}
