﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Circle : MonoBehaviour
{
    [SerializeField]
    GameObject projectilePrefab;

    Enemy enemy;
    int level = 1;
    float cost = 5;
    float timer = 0;
    Text text;
    Animator anim;

    void Start()
    {
        enemy = GameObject.FindObjectOfType<Enemy>();
        text = transform.GetChild(0).GetComponent<Text>();
        anim = GetComponent<Animator>();
        text.text = cost.ToString();
    }

    private void Update()
    {
        if (timer <= 0f)
        {
            PlayerStats.TryToSpendMoney(-5 * (Mathf.Pow(level, 2.1f)));
            anim.SetTrigger("Shoot");
            if (projectilePrefab)
            {
                Vector3 targetPos = Camera.main.ScreenToWorldPoint(enemy.transform.position) + new Vector3(0, 0, 10f);
                GameObject projectile = Instantiate(projectilePrefab, Camera.main.ScreenToWorldPoint(transform.position) + new Vector3(0, 0, 10f), Quaternion.identity);
                projectile.transform.LookAt(targetPos, Vector3.up);
                projectile.GetComponent<Projectile>().targetPos = targetPos;
            }
            timer = 1f;
        }
        else
        {
            timer -= Time.deltaTime;
        }
    }

    public void Upgrade()
    {
        if (PlayerStats.TryToSpendMoney(cost))
        {
            level++;
            cost = 5 * (Mathf.Pow(1.08f, level));
            text.text = cost.ToString("F0");
        }
    }
}
