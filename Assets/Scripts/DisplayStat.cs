﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayStat : MonoBehaviour
{

    enum StatType
    {
        Gold,
        Level
    }

    [SerializeField]
    StatType statType;

    Text text;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
        switch (statType)
        {
            case StatType.Gold:
                PlayerStats.goldChanged += GoldChanged;
                break;
            case StatType.Level:
                PlayerStats.levelChanged += LevelChanged;
                break;
            default:
                Debug.Log("This shouldn't happen...");
                break;
        }
    }

    void GoldChanged(object sender, EventArgs args)
    {
        text.text = "Gold: " + PlayerStats.gold.ToString("F0");
    }

    void LevelChanged(object sender, EventArgs args)
    {
        text.text = "Level: " + PlayerStats.upgradeLevel.ToString();
    }
}
