﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public void Tap()
    {
        PlayerStats.TryToSpendMoney(-5 * (Mathf.Pow(PlayerStats.upgradeLevel, 2.1f)));
    }
}
