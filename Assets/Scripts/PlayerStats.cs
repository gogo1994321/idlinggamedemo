﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerStats
{
    //Not specified in email, so since the increase levels aren't full number multiplications, for simplicity sake these are floats
    public static float gold;
    public static float upgradeLevel = 1f;

    public static EventHandler goldChanged;
    public static EventHandler levelChanged;

    public static bool TryToSpendMoney(float amount)
    {
        if (amount <= gold)
        {
            gold -= amount;
            goldChanged.Invoke(null, null);
            return true;
        }

        return false;
    }

    public static void UpgradeLevel()
    {
        upgradeLevel++;
        levelChanged.Invoke(null, null);
    }
}
