﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [HideInInspector]
    public Vector3 targetPos;
    public float speed;

    void Update()
    {
        Vector3 dir = targetPos - transform.position;
        if (dir.magnitude < 0.1f)
        {
            Destroy(gameObject);
        }
        else
        {
            transform.position += dir.normalized * Time.deltaTime * speed;
        }
    }
}
