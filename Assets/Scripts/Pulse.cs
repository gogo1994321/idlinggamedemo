﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulse : MonoBehaviour
{
    public float pulseSpeed = 10f;
    public float scaler = 0.9f;

    Vector3 originalScale;

    private void Start()
    {
        originalScale = transform.localScale;
    }

    public void PulseOnce()
    {
        //Make sure we reset to correct start scale since user might spam it
        transform.localScale = originalScale;
        StartCoroutine(DoPulse());
    }

    IEnumerator DoPulse()
    {
        float t = 0;
        Vector3 startSize = transform.localScale;

        while (t < 1f)
        {
            transform.localScale = Vector3.Lerp(startSize, startSize * scaler, t);

            t += Time.deltaTime * pulseSpeed;
            yield return null;
        }
        while (t > 0f)
        {
            transform.localScale = Vector3.Lerp(startSize, startSize * scaler, t);

            t -= Time.deltaTime * pulseSpeed;
            yield return null;
        }
        transform.localScale = originalScale;
    }
}
