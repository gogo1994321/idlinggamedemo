﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeTap : MonoBehaviour
{
    Text text;
    float upgradeCost = 5;

    void Start()
    {
        text = transform.GetChild(0).GetComponent<Text>();
    }

    public void Tap()
    {
        if (PlayerStats.TryToSpendMoney(upgradeCost))
        {
            PlayerStats.UpgradeLevel();
            upgradeCost = 5 * (Mathf.Pow(1.08f, PlayerStats.upgradeLevel));
            text.text = "Upgrade Tap\nCost: " + upgradeCost.ToString("F0");
        }
    }
}
